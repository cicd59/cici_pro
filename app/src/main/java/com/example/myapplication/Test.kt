package com.example.myapplication
import java.io.*
import java.math.*
import java.security.*
import java.text.*
import java.util.*
import java.util.concurrent.*
import java.util.function.*
import java.util.regex.*
import java.util.stream.*
import kotlin.collections.*
import kotlin.comparisons.*
import kotlin.io.*
import kotlin.jvm.*
import kotlin.jvm.functions.*
import kotlin.jvm.internal.*
import kotlin.ranges.*
import kotlin.sequences.*
import kotlin.text.*

// Complete the minimumSwaps function below.
fun minimumSwaps(arr: Array<Int>): Int {

    var swap =0
    var isSwap = false

    for(index in arr.indices){
        var min = index
        isSwap=true

        for(j in index+1 until arr.size){
            if(arr[j]<arr[min]){
                min = j
                isSwap=false
            }
        }
        if(isSwap){
            return swap
        }

        var temp = arr[index]
        arr[index]=arr[min]
        arr[min]=temp
        swap++
    }
    return  swap
}

fun main(args: Array<String>) {
//    val scan = Scanner(System.`in`)
//
//    val n = scan.nextLine().trim().toInt()
//
//    val arr = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()
//
    val arr=arrayOf(7,3,6,1,9,2)
    val res = minimumSwaps(arr)
    println("Swap:  $res")
    println("Sorted Array: ${arr.toList()}")
}